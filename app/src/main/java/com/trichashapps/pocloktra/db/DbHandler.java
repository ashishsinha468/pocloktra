package com.trichashapps.pocloktra.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import com.trichashapps.pocloktra.db.daos.LocationsDao;

import java.util.List;

/**
 * Created by ashish on 19/4/17.
 */

public class DbHandler extends SQLiteOpenHelper {

    private static DbHandler instance;

    private DbHandler(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    public static synchronized DbHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DbHandler(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBConstants.CREATE_QUERIES.CREATE_TABLE_LOCATION_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBConstants.DROP_QUERIES.LOCATION_DATA);
        onCreate(db);
    }

    public void dropAllTables() {
        try {
            clearTable(DBConstants.LOCATION.TABLE_NAME);
        } catch (DbCRUDException e) {
            e.printStackTrace();
        }
    }


    /**
     * CRUDs
     */

    public void clearTable(String tableName) throws DbCRUDException {
        new LocationsDao(this.getWritableDatabase()).clearTable();
    }


    public void saveLocation(Location location) throws DbCRUDException {
        new LocationsDao(this.getWritableDatabase()).addLocation(location);
    }

    public List<Location> getLocations() throws DbCRUDException {
        return new LocationsDao(this.getWritableDatabase()).getLocations();
    }
}

