package com.trichashapps.pocloktra.db;

/**
 * Created by ashish on 19/4/17.
 */
public class DBConstants {
    public static final String DB_NAME = "pocloktra";
    public static final int DB_VERSION = 4;

    public static class LOCATION {
        public static final String KEY_DATA = "data";
        public static String TABLE_NAME = "LocationData";
        public static String KEY_ID = "id";
    }

    public static class CREATE_QUERIES {
        public static final String CREATE_TABLE_LOCATION_DATA = "CREATE TABLE " + LOCATION.TABLE_NAME + " ( " + LOCATION.KEY_ID + " INTEGER PRIMARY KEY, " + LOCATION.KEY_DATA + " TEXT )";
    }

    public static class DROP_QUERIES {
        public static final String LOCATION_DATA = "DROP TABLE IF EXISTS " + LOCATION.TABLE_NAME;
    }
}
