package com.trichashapps.pocloktra.db.interfaces;

import android.location.Location;

import com.trichashapps.pocloktra.db.DbCRUDException;

import java.util.List;


/**
 * Created by ashish on 19/4/17.
 */

public interface LocationActions {
    void addLocation(Location location) throws DbCRUDException;

    List<Location> getLocations() throws DbCRUDException;

    void clearTable() throws DbCRUDException;
}
