package com.trichashapps.pocloktra.db.daos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

import com.trichashapps.pocloktra.db.DBConstants;
import com.trichashapps.pocloktra.db.DbCRUDException;
import com.trichashapps.pocloktra.db.interfaces.LocationActions;
import com.trichashapps.pocloktra.utils.GsonUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ashish on 19/4/17.
 */

public class LocationsDao implements LocationActions {
    private String TABLE_NAME;
    private SQLiteDatabase db;

    public LocationsDao(SQLiteDatabase writableDatabase) {
        this.db = writableDatabase;
        this.TABLE_NAME = DBConstants.LOCATION.TABLE_NAME;
    }


    private void insertLocationsIntoDb(Location location) throws DbCRUDException {
        try {
            ContentValues values = new ContentValues();
            values.put(DBConstants.LOCATION.KEY_ID, location.getTime());
            values.put(DBConstants.LOCATION.KEY_DATA, GsonUtils.getJsonString(location));
            db.insert(TABLE_NAME, null, values);
//            db.close();
        } catch (Exception e) {
            throw new DbCRUDException(e);
        }
    }


    @Override
    public void addLocation(Location location) throws DbCRUDException {
        insertLocationsIntoDb(location);
    }

    @Override
    public List<Location> getLocations() throws DbCRUDException {
        List<Location> locations;
        try {
            locations = new ArrayList<>();
            String QUERY = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + DBConstants.LOCATION.KEY_ID + " DESC";
            Cursor cursor = db.rawQuery(QUERY, null);
            if (!cursor.isLast()) {
                while (cursor.moveToNext()) {
                    Location location = GsonUtils.getObjectFromJson(cursor.getString(1), Location.class);
                    locations.add(location);
                }
            }
            cursor.close();
//            db.close();
        } catch (Exception e) {
            throw new DbCRUDException(e);

        }
        return locations;
    }

    @Override
    public void clearTable() throws DbCRUDException {
        String query = "delete from " + TABLE_NAME;
        try {
            db.execSQL(query);
        } catch (Exception e) {
            throw new DbCRUDException(e);
        }
    }
}
