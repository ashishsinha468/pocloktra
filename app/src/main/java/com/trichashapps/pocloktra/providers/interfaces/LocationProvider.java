package com.trichashapps.pocloktra.providers.interfaces;

import android.content.Context;
import android.location.Location;

import com.trichashapps.pocloktra.application.config.LocationParams;
import com.trichashapps.pocloktra.utils.interfaces.Logger;


/**
 * Created by ashish on 26/4/17.
 */

public interface LocationProvider {

    void init(Context context, Logger logger);

    void start(OnLocationUpdatedListener listener, LocationParams params, boolean singleUpdate);

    void stop();

    Location getLastLocation();
}
