package com.trichashapps.pocloktra.providers.interfaces;

import android.location.Location;

/**
 * Created by ashish on 26/4/17.
 */

public interface OnLocationUpdatedListener {
    void onLocationUpdated(Location location);
}
