package com.trichashapps.pocloktra.providers.interfaces;


import com.trichashapps.pocloktra.utils.interfaces.ServiceConnectionListener;

/**
 * Created by ashish on 26/4/17.
 */

public interface ServiceLocationProvider extends LocationProvider {
    ServiceConnectionListener getServiceListener();

    /**
     * Set the {@link ServiceConnectionListener} used for callbacks from the 3rd party service.
     *
     * @param listener a <code>ServiceConnectionListener</code> to respond to connection events from
     *                 the underlying 3rd party location service.
     */
    void setServiceListener(ServiceConnectionListener listener);
}
