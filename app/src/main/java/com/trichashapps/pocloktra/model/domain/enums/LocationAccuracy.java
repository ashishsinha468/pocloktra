package com.trichashapps.pocloktra.model.domain.enums;

/**
 * Created by ashish on 26/4/17.
 */

public enum LocationAccuracy {
    LOWEST,
    LOW,
    MEDIUM,
    HIGH
}
