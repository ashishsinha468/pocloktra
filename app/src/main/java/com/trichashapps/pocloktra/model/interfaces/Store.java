package com.trichashapps.pocloktra.model.interfaces;

/**
 * Created by ashish on 26/4/17.
 */

public interface Store<T> {
    T get(String key);
    void put(String key, T value);
    void remove(String key);
}
