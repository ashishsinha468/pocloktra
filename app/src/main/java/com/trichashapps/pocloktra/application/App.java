package com.trichashapps.pocloktra.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by ashish on 27/4/17.
 */

public class App extends Application {
    private static Context context;

    public static Context getAppContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
    }
}
