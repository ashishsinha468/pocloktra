package com.trichashapps.pocloktra.application;

/**
 * Created by ashish on 27/4/17.
 */

public class Constants {

    public static final String PREFERENCES_FILENAME = "pocLoktra";

    public class BUNDLE_KEYS {
        public static final String SHIFT_STARTED_AT = "shiftStartedAt";
        public static final String isShiftStarted = "isShiftStarted";
        public static final String SHIFT_ENDED_AT = "shiftEndedAt";
    }
}
