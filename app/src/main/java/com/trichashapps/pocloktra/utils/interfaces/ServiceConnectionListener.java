package com.trichashapps.pocloktra.utils.interfaces;

/**
 * Created by ashish on 26/4/17.
 */

public interface ServiceConnectionListener {

    /**
     * Callback when a successful connection to a 3rd party service is made
     */
    void onConnected();

    /**
     * Callback when the connection to a 3rd party service is interrupted (network failure,
     * temporary outage, etc.)
     */
    void onConnectionSuspended();

    /**
     * Callback when the connection to a 3rd party service fails (missing libraries, bad API key,
     * etc.)
     */
    void onConnectionFailed();
}
