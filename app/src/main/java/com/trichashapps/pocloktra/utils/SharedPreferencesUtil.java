package com.trichashapps.pocloktra.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.trichashapps.pocloktra.application.App;
import com.trichashapps.pocloktra.application.Constants;

/**
 * Created by ashish on 27/4/17.
 */

public class SharedPreferencesUtil {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static SharedPreferencesUtil sharedPreferencesUtil;

    private static void initializeSharedPref(Context context) {
        sharedPreferencesUtil = new SharedPreferencesUtil();
        sharedPreferencesUtil.sharedPreferences = context.getSharedPreferences(
                Constants.PREFERENCES_FILENAME, Activity.MODE_PRIVATE);
        sharedPreferencesUtil.editor = sharedPreferencesUtil.sharedPreferences
                .edit();
    }

    public static SharedPreferencesUtil getInstance() {
        if (sharedPreferencesUtil == null) {
            initializeSharedPref(App.getAppContext());
        }
        return sharedPreferencesUtil;
    }

    public synchronized boolean saveData(String key, String value) {
        editor.putString(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, boolean value) {
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, long value) {
        editor.putLong(key, value);
        return editor.commit();
    }


    public synchronized boolean saveData(String key, float value) {
        editor.putFloat(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, int value) {
        editor.putInt(key, value);
        return editor.commit();
    }

    public synchronized boolean removeData(String key) {
        editor.remove(key);
        return editor.commit();
    }

    public synchronized Boolean getData(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public synchronized String getData(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public synchronized float getData(String key, float defaultValue) {

        return sharedPreferences.getFloat(key, defaultValue);
    }

    public synchronized int getData(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public synchronized long getData(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

    public synchronized void deleteAllData() {
        sharedPreferencesUtil = null;
        editor.clear();
        editor.commit();
    }

}

