package com.trichashapps.pocloktra.utils;

import android.app.Service;

import com.trichashapps.pocloktra.application.Constants;
import com.trichashapps.pocloktra.services.LocationTrackerService;

/**
 * Created by ashish on 26/4/17.
 */

public class MiscUtils {
    public static boolean doesServiceNeedsToBeRestarted(Service service) {
        if (service.getClass().equals(LocationTrackerService.class)) {
            if (isFeasibleToRestartLocationService()) {
                return true;
            }
            return false;
        }
        return false;
    }

    private static boolean isFeasibleToRestartLocationService() {
        return SharedPreferencesUtil.getInstance().getData(Constants.BUNDLE_KEYS.isShiftStarted, false);
    }
}
