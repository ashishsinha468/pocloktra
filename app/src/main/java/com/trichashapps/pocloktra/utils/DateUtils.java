package com.trichashapps.pocloktra.utils;

import com.trichashapps.pocloktra.R;
import com.trichashapps.pocloktra.application.App;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ashish on 27/4/17.
 */

public class DateUtils {

    public static String getHumanReadableDate(DateTime dateTime) {
        return DateTimeFormat.forPattern(App.getAppContext().getString(R.string.date_format_human)).print(dateTime);
    }

    public static String getCurrentDateTimeInHumanReadableFormat() {
        return getHumanReadableDate(new DateTime().withMillis(getCurrentMillis()));
    }

    public static String getHumanReadableDate(DateTime dateTime, String dateFormat) {
        DateTimeFormatter df = DateTimeFormat.forPattern(dateFormat);
        return df.print(dateTime);
    }

    public static String getFieldEZDateTimeString(DateTime dateTime) {
        return DateTimeFormat.forPattern(App.getAppContext().getString(R.string.date_format_human_mini)).print(dateTime);
    }

    public static String getDateAndTimeFromTimestampString(String str) {
        Long timestamp = Long.parseLong(str);
        Date date = new Date(timestamp);
        DateFormat format = new SimpleDateFormat(App.getAppContext().getString(R.string.date_format_human_mini));
        String formatted = format.format(date);
        return formatted;
    }

    public static String getDateFromTimestampString(String str) {
        Long timestamp = Long.parseLong(str);
        Date date = new Date(timestamp);
        DateFormat format = new SimpleDateFormat(App.getAppContext().getString(R.string.date_format_human_mini));
        String formatted = format.format(date);
        return formatted;
    }

    public static int getHourDifference(long current, long last) {
        DateTime currentDate = new DateTime(current);
        DateTime previousDate = new DateTime(last);
        Hours hours = Hours.hoursBetween(previousDate, currentDate);
        return hours.getHours();
    }

    public static int getMinuteDifference(long current, long last) {
        DateTime currentDate = new DateTime(current);
        DateTime previousDate = new DateTime(last);
        Minutes minutes = Minutes.minutesBetween(previousDate, currentDate);
        return minutes.getMinutes();
    }

    public static String getCurrentTimestamp() {
        long millis = new Date().getTime();
        return String.valueOf(millis);
    }

    public static long getCurrentMillis() {
        return new Date().getTime();
    }

    public static String getHoursAndMinutesDifferenceBetweenTwoTimestamps(long start, long end) {
        long diff = end - start;
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);

        return diffHours + " Hours " + " " + diffMinutes + " Minutes";
    }

}

