package com.trichashapps.pocloktra.utils;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.trichashapps.pocloktra.application.config.LocationParams;
import com.trichashapps.pocloktra.providers.LocationGooglePlayServicesWithFallbackProvider;
import com.trichashapps.pocloktra.providers.interfaces.LocationProvider;
import com.trichashapps.pocloktra.providers.interfaces.OnLocationUpdatedListener;
import com.trichashapps.pocloktra.utils.interfaces.Logger;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by ashish on 26/4/17.
 */


public class LocationDriver {

    private Context context;
    private Logger logger;
    private boolean preInitialize;

    /**
     * Creates the SmartLocation basic instance.
     *
     * @param context       execution context
     * @param logger        logger interface
     * @param preInitialize TRUE (default) if we want to instantiate directly the default providers. FALSE if we want to initialize them ourselves.
     */
    private LocationDriver(Context context, Logger logger, boolean preInitialize) {
        this.context = context;
        this.logger = logger;
        this.preInitialize = preInitialize;
    }

    public static LocationDriver with(Context context) {
        return new Builder(context).build();
    }

    /**
     * @return request handler for location operations
     */
    public LocationControl location() {
        return location(new LocationGooglePlayServicesWithFallbackProvider(context));
    }

    /**
     * @param provider location provider we want to use
     * @return request handler for location operations
     */
    public LocationControl location(LocationProvider provider) {
        return new LocationControl(this, provider);
    }

    /**
     * Builder for activity recognition. Use activity() instead.
     *
     * @return builder for activity recognition.
     * @deprecated
     */

    public static class Builder {
        private final Context context;
        private boolean loggingEnabled;
        private boolean preInitialize;

        public Builder(@NonNull Context context) {
            this.context = context;
            this.loggingEnabled = false;
            this.preInitialize = true;
        }

        public Builder logging(boolean enabled) {
            this.loggingEnabled = enabled;
            return this;
        }

        public Builder preInitialize(boolean enabled) {
            this.preInitialize = enabled;
            return this;
        }

        public LocationDriver build() {
            return new LocationDriver(context, LoggerFactory.buildLogger(loggingEnabled), preInitialize);
        }

    }

    public static class LocationControl {

        private static final Map<Context, LocationProvider> MAPPING = new WeakHashMap<>();

        LocationDriver locationDriver;
        private LocationParams params;
        private LocationProvider provider;
        private boolean oneFix;

        public LocationControl(@NonNull LocationDriver locationDriver, @NonNull LocationProvider locationProvider) {
            this.locationDriver = locationDriver;
            params = LocationParams.NAVIGATION;
            oneFix = false;

            if (!MAPPING.containsKey(locationDriver.context)) {
                MAPPING.put(locationDriver.context, locationProvider);
            }
            provider = MAPPING.get(locationDriver.context);

            if (locationDriver.preInitialize) {
                provider.init(locationDriver.context, locationDriver.logger);
            }
        }

        public LocationControl config(@NonNull LocationParams params) {
            this.params = params;
            return this;
        }

        public LocationControl oneFix() {
            this.oneFix = true;
            return this;
        }

        public LocationControl continuous() {
            this.oneFix = false;
            return this;
        }

        public LocationState state() {
            return LocationState.with(locationDriver.context);
        }

        @Nullable
        public Location getLastLocation() {
            return provider.getLastLocation();
        }

        public LocationControl get() {
            return this;
        }

        public void start(OnLocationUpdatedListener listener) {
            if (provider == null) {
                throw new RuntimeException("A provider must be initialized");
            }
            provider.start(listener, params, oneFix);
        }

        public void stop() {
            provider.stop();
        }
    }

}

