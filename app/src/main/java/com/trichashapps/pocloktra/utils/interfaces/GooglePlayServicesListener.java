package com.trichashapps.pocloktra.utils.interfaces;

/**
 * Created by ashish on 26/4/17.
 */

public interface GooglePlayServicesListener {
    void onConnected(android.os.Bundle bundle);

    void onConnectionSuspended(int i);

    void onConnectionFailed(com.google.android.gms.common.ConnectionResult connectionResult);
}
