package com.trichashapps.pocloktra.ui;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trichashapps.pocloktra.R;
import com.trichashapps.pocloktra.application.App;
import com.trichashapps.pocloktra.application.Constants;
import com.trichashapps.pocloktra.db.DBConstants;
import com.trichashapps.pocloktra.db.DbCRUDException;
import com.trichashapps.pocloktra.db.DbHandler;
import com.trichashapps.pocloktra.services.LocationTrackerService;
import com.trichashapps.pocloktra.utils.DateUtils;
import com.trichashapps.pocloktra.utils.SharedPreferencesUtil;
import com.trichashapps.pocloktra.utils.SimpleGestureFilter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity implements SimpleGestureFilter.SimpleGestureListener {

    @InjectView(R.id.fl_container_map)
    FrameLayout flContainerMap;

    @InjectView(R.id.cv_container_left_right_swipe)
    CardView cvContainerLeftRightSwipe;

    @InjectView(R.id.cv_container_shift_duration)
    CardView cvContainerShiftDuration;

    @InjectView(R.id.tv_total_shift_time_value)
    TextView tvTotalShiftTimeValue;

    @InjectView(R.id.tv_swipe_left)
    TextView tvSwipeLeft;

    @InjectView(R.id.tv_swipe_right)
    TextView tvSwipeRight;

    @InjectView(R.id.tv_swipe_display_text)
    TextView tvSwipeDisplayText;

    public static GoogleMap googleMap;

    private static final int LOCATION_PERMISSION_ID = 1001;
    private MapFragment customMapFragment;
    private PolylineOptions polyLineOptions;

    private SimpleGestureFilter detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        init();
    }

    private void init() {
        toggleUiForShiftStates();
        detector = new SimpleGestureFilter(this, this);
        polyLineOptions = new PolylineOptions();
        initMapFragment();
        initCardViewSwipe();
    }

    private void toggleUiForShiftStates() {
        boolean isShiftStarted = SharedPreferencesUtil.getInstance().getData(Constants.BUNDLE_KEYS.isShiftStarted, false);
        if (isShiftStarted) {
            tvSwipeDisplayText.setText(getString(R.string.label_swipe_stop_display_text));
            cvContainerLeftRightSwipe.setBackgroundColor(getResources().getColor(R.color.color_green));
            tvSwipeRight.setVisibility(View.VISIBLE);
            tvSwipeLeft.setVisibility(View.GONE);
            cvContainerShiftDuration.setVisibility(View.GONE);
        } else {
            tvSwipeDisplayText.setText(getString(R.string.label_swipe_start_display_text));
            cvContainerLeftRightSwipe.setBackgroundColor(getResources().getColor(R.color.color_red));
            tvSwipeRight.setVisibility(View.GONE);
            tvSwipeLeft.setVisibility(View.VISIBLE);
            cvContainerShiftDuration.setVisibility(View.VISIBLE);
            tvTotalShiftTimeValue.setText(getShiftTimeValue());
        }
    }

    private String getShiftTimeValue() {
        long startTime = SharedPreferencesUtil.getInstance().getData(Constants.BUNDLE_KEYS.SHIFT_STARTED_AT, -1l);
        long endTime = SharedPreferencesUtil.getInstance().getData(Constants.BUNDLE_KEYS.SHIFT_ENDED_AT, -1l);
        String diff = DateUtils.getHoursAndMinutesDifferenceBetweenTwoTimestamps(startTime, endTime);
        return diff;
    }

    private void initCardViewSwipe() {
        cvContainerLeftRightSwipe.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });
    }

    private void initMapFragment() {
        customMapFragment = new MapFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_container_map, customMapFragment);
        transaction.commit();


    }

    private void setUpMap() {
        googleMap.setTrafficEnabled(true);
        getLocationFromDb();
    }

    private void getLocationFromDb() {
        DbHandler instance = DbHandler.getInstance(this);
        try {
            List<Location> locations = instance.getLocations();
            List<LatLng> latLngsList = new ArrayList<>();
            for (Location location : locations) {
                latLngsList.add(new LatLng(location.getLatitude(), location.getLongitude()));
            }
            polyLineOptions.addAll(latLngsList);
            googleMap.addPolyline(polyLineOptions);
            float zoomLevel = 20.0f;
            if (!latLngsList.isEmpty())
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngsList.get(0), zoomLevel));


        } catch (DbCRUDException e) {
            e.printStackTrace();
        }

    }


    private void askUserForLocationPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_ID);
    }

    private boolean userHasGivenLocationPermission() {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startLocationService();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void startLocationService() {
        clearLocationData();
        SharedPreferencesUtil.getInstance().saveData(Constants.BUNDLE_KEYS.SHIFT_STARTED_AT, DateUtils.getCurrentMillis());
        SharedPreferencesUtil.getInstance().saveData(Constants.BUNDLE_KEYS.isShiftStarted, true);
        Intent intent = LocationTrackerService.newIntent(this);
        startService(intent);
        toggleUiForShiftStates();
    }

    private void clearLocationData() {
        try {
            DbHandler.getInstance(App.getAppContext()).clearTable(DBConstants.LOCATION.TABLE_NAME);
        } catch (DbCRUDException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                stopLocationUpdates();
                showShiftDetailsOnUi();
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                if (!userHasGivenLocationPermission()) {
                    askUserForLocationPermission();
                } else {
                    startLocationService();
                }
                break;
        }
    }

    private void showShiftDetailsOnUi() {
        customMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                MainActivity.googleMap = googleMap;
                setUpMap();
            }
        });
        toggleUiForShiftStates();


    }

    private void stopLocationUpdates() {
        SharedPreferencesUtil.getInstance().saveData(Constants.BUNDLE_KEYS.isShiftStarted, false);
        Intent intent = LocationTrackerService.newIntent(this);
        stopService(intent);

    }

}
