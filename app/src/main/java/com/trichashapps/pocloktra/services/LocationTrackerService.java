package com.trichashapps.pocloktra.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.trichashapps.pocloktra.application.Constants;
import com.trichashapps.pocloktra.db.DbCRUDException;
import com.trichashapps.pocloktra.db.DbHandler;
import com.trichashapps.pocloktra.providers.LocationGooglePlayServicesProvider;
import com.trichashapps.pocloktra.providers.interfaces.OnLocationUpdatedListener;
import com.trichashapps.pocloktra.utils.DateUtils;
import com.trichashapps.pocloktra.utils.LocationDriver;
import com.trichashapps.pocloktra.utils.MiscUtils;
import com.trichashapps.pocloktra.utils.SharedPreferencesUtil;

/**
 * Created by ashish on 26/4/17.
 */

public class LocationTrackerService extends Service implements OnLocationUpdatedListener {
    private LocationGooglePlayServicesProvider provider;
    private DbHandler instance;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, LocationTrackerService.class);
        return intent;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        System.out.println("I was recreated");
        instance = DbHandler.getInstance(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startLocation();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onLocationUpdated(Location location) {
        boolean isShiftStarted = SharedPreferencesUtil.getInstance().getData(Constants.BUNDLE_KEYS.isShiftStarted, false);
        if (isShiftStarted) {
            insertLocationDetailsInDb(location);
            Toast.makeText(this, "Present Location is " + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();
            System.out.println("Present Location is " + location.getLatitude() + " " + location.getLongitude());
        }
    }

    private void startLocation() {
        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);

        LocationDriver locationDriver = new LocationDriver.Builder(this).logging(true).build();
        locationDriver.location(provider).start(this);
    }

    private void insertLocationDetailsInDb(Location location) {
        try {
            instance.saveLocation(location);
        } catch (DbCRUDException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (MiscUtils.doesServiceNeedsToBeRestarted(this)) {
            Intent intent = LocationTrackerService.newIntent(this);
            startService(intent);
        } else {
            stopSelf();
            SharedPreferencesUtil.getInstance().saveData(Constants.BUNDLE_KEYS.SHIFT_ENDED_AT, DateUtils.getCurrentMillis());
            SharedPreferencesUtil.getInstance().saveData(Constants.BUNDLE_KEYS.isShiftStarted, false);
        }
        System.out.println("I was destroyed");
    }
}
